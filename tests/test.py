# -*- coding: utf-8 -*-

#from .context import Path

from context import roomgenerator as rg

import unittest
import numpy as np



class BasicTestSuite(unittest.TestCase):
    """Basic test cases."""

    def test_box(self):
        
        p0 = rg.Point(-1.24, -1.7, -1.9)
        p1 = rg.Point(np.pi, 2*np.pi, 3*np.pi)
        box = rg.Box(p0, p1)

        box.write_stl("box.stl")
        bounding = box.get_bounding_box()
        
        #self.assertTrue(p1.is_overlapping(p2))

    def test_cone(self):
        
        p0 = rg.Point(-1.24, -1.7, -1.9)
        p1 = rg.Point(np.pi, 2*np.pi, 3*np.pi)
        r0 = 0.5
        r1 = 1.0
        cone = rg.Cone(p0, p1, r0, r1)

        cone.write_stl("./cone.stl")
        bounding = box.get_bounding_box()
        
        #self.assertTrue(p1.is_overlapping(p2))


    def test_room(self):
        
        Lx = 10.0; Ly = 10.0; Lz = 3.0;
        room = rg.Room(Lx, Ly, Lz)

        p0 = rg.Point(-1.24, -1.7, -1.9)
        p1 = rg.Point(np.pi, 2*np.pi, 3*np.pi)
        box = rg.Box(p0, p1, id="Aisle_1")
        room.add_obstacle(box)

        room.write_stl("./")
        
        #self.assertTrue(p1.is_overlapping(p2))
       
if __name__ == '__main__':
    unittest.main()



