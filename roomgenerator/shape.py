# -*- coding: utf-8 -*-



class Shape(object):

    def __init__(self, id):
        self.id = id

    def write_stl(self, path):
        """
        This mimics a pure virtual function. All shapes must define this.
        """    

        raise NotImplementedError('write_stl() not defined!')

    def get_bounding_box(self):
        """
        This mimics a pure virtual function. All shapes must define this.
        """
        raise NotImplementedError('get_bounding_box() not defined!')


    def is_overlapping(self, other):
        
        my_bbox = self.get_bounding_box()
        other_bbox = other.get_bounding_box()

        #... do stuff
        

        return False
