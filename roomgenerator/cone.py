# -*- coding: utf-8 -*-

from .shape import Shape



class Cone(Shape):
    
    """
    Construct from two points and the corresponding radius. For a cylinder r0 = r1.
    """
    def __init__(self, p0, p1, r0, r1):
        
        self.p0 = p0; self.p1 = p1;
        self.r0 = r0; self.r1 = r1;


    
    """
    def write_stl(self, path):
    
        pass
    
    
    

    def get_bounding_box(self):
        pass

    """
