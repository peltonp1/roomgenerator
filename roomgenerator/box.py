# -*- coding: utf-8 -*-

import numpy as np
#import pylab as pl

from .shape import Shape
from .point import Point

from stl import mesh


class Box(Shape):
    
    """
    Construct from two points and a possible rotation vector around axes x,y,z.
    The rotation angle is in degrees.
    """
    def __init__(self, p0, p1, rotate=np.array([0.0, 0.0, 0.0]), id=""):
            
        Shape.__init__(self, id)
        
        self.p0 = p0; self.p1 = p1;
        self.rotate = rotate

    
    
    def write_stl(self, path):
    
        xmin = self.p0.x; ymin = self.p0.y; zmin = self.p0.z
        xmax = self.p1.x; ymax = self.p1.y; zmax = self.p1.z
        
        vertices = np.array([\
            [xmin, ymin, zmin],
            [xmax, ymin, zmin],
            [xmax, ymax, zmin],
            [xmin, ymax, zmin],
            [xmin, ymin, zmax],
            [xmax, ymin, zmax],
            [xmax, ymax, zmax],
            [xmin, ymax, zmax]])
    
        faces = np.array([\
            [0,3,1],
            [1,3,2],
            [0,4,7],
            [0,7,3],
            [4,5,6],
            [4,6,7],
            [5,1,2],
            [5,2,6],
            [2,3,6],
            [3,7,6],
            [0,1,5],
            [0,5,4]])

        cube = mesh.Mesh(np.zeros(faces.shape[0], dtype=mesh.Mesh.dtype))
        for i, f in enumerate(faces):
            for j in range(3):
                cube.vectors[i][j] = vertices[f[j],:]
        

        string = "{0}/{1}.stl".format(path, self.id)
        
        cube.save(string)




    def get_bounding_box(self):
        return self


    def get_center(self):

        cvec = 0.5 * (self.p0.to_vector() + self.p1.to_vector())
        return Point(cvec[0], cvec[1], cvec[2])


    














