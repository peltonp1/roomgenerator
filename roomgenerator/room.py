# -*- coding: utf-8 -*-

import numpy as np
import pylab as pl
import matplotlib.patches as patches

from .shape import Shape
from .box   import Box
from .point import Point

#maybe derive from Box directly
class Room(Shape):

    def __init__(self, Lx, Ly, Lz):
        
        self.Lx = Lx; self.Ly = Ly; self.Lz = Lz;
    
        self.p0 = Point(-0.5*Lx, -0.5*Ly, 0.0) #note always z=0!
        self.p1 = Point(0.5*Lx, 0.5*Ly, Lz) #note always z=0

        
        self.walls = Box(self.p0, self.p1, id="walls")
        
        self.obstacles = []

    
    def write_stl(self, path):
         
        self.walls.write_stl(path)
        
        for obstacle in self.obstacles:
            
            obstacle.write_stl(path)
    

    def get_bounding_box(self):
        return self.walls    


    def add_obstacle(self, obstacle):
        
        if (self.__can_add(obstacle)):
            self.obstacles.append(obstacle)
    
    def plot_floorplan(self):

        fig,ax = pl.subplots(1)        
    
        for obstacle in self.obstacles:
            bbox = obstacle.get_bounding_box()
            #note this is the lower left (i.e. p0)
            xy = (bbox.p0.x, bbox.p0.y)
            Lx = bbox.p1.x - bbox.p0.x
            Ly = bbox.p1.y - bbox.p0.y
        
            poly = patches.Rectangle(xy, Lx, Ly)

            ax.add_patch(poly)

        pl.xlim(self.p0.x, self.p1.x)
        pl.ylim(self.p0.y, self.p1.y)
        ax.set_aspect(aspect='equal')
        pl.show()

    def __can_add(self, obstacle):
        
        #for now...
        return True

    
    
 


