
# -*- coding: utf-8 -*-

# Learn more: https://github.com/kennethreitz/setup.py

from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

setup(
    name='roomgenerator',
    version='0.1.0',
    description='Utility to create a room for virus spreading simulations',
    long_description=readme,
    author='Petteri Peltonen',
    author_email='petteri.peltonen@aalto.fi',
    url='https://peltonp1@bitbucket.org/peltonp1/roomgenerator.git',
    #license=license,
    packages=find_packages(exclude=('tests'))
)
